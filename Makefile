OPTIONS = --template=styles/template.tex --filter pandoc-crossref
CONFIG  = --metadata-file config.yml
BIB     = --filter pandoc-citeproc --bibliography=references.bib
SRC     = $(shell ls $(SRC_DIR)/*.md)
SRC_DIR = sections
PROJECT = LI3_1819_report-java

$(PROJECT):
	pandoc $(CONFIG) $(OPTIONS) $(BIB) -s $(SRC) -o $(PROJECT).pdf

clean:
	@echo "Cleaning..."
	@-cat .art/maid.ascii
	@rm $(PROJECT).pdf
	@echo "...✓ done!"
