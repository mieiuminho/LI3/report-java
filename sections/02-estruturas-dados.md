# Estruturas de dados {#sec:estruturas}

## Venda

```java
private IProduto produto;
private double preco;
private int quantidade;
private String tipo;
private ICliente cliente;
private int mes;
private int filial;
```
A presente estrutura representa uma linha do ficheiro de vendas. À medida que
são lidas linhas, os dados são introduzidos na presente estrutura e
posteriormente validados. Assim, a estrutura é meramente utilizada como meio de
validação de dados. Esta classe implementa a interface `IVenda`.

## Cliente

```java
private String codigo;
```

Esta estrutura possui uma _string_ com o código de cliente e implementa a
interface `ICliente`.

## Produto

```java
private String codigo;
```

Esta estrutura possui uma _string_ com o código de produto e implementa a
interface `IProduto`.

## Catálogo de Clientes

```java
private Set<ICliente> clientes;
private int usados;
```

Esta estrutura possui um inteiro que representa o número de inteiro que
realizaram transações. Conta também com um `Set` de clientes, que contém as
estruturas `ICliente` de todos os clientes registados. Esta classe implementa a
interface `ICatCliente`.

## Catálogo de Produtos

```java
private Set<IProduto> produtos;
private int usados;
```

Esta estrutura possui um inteiro que representa o número de produtos que foram
incluídos em transações. Conta também com um `Set` de produtos, que contém as
estruturas `IProduto` de todos os produtos registados. Esse classe implementa a
interface `ICatProduto`.

## Faturação Produto

```java
private String codigoProduto;
private double faturadoN;
private double faturadoP;
private int unidadesN;
private int unidadesP;
private int registosVendaN;
private int registosVendaP;
```

Esta estrutura contém quatro inteiros e dois `double`. Primeiro contém o código
do produto a que a presente estrutura de `FaturacaoProduto` diz respeito. Contém
o valor faturado com vendas do produto em regime normal e em regime promocional
(separadamente). Conta ainda com inteiros que registam o número de unidades do
presente produto transacionadas em regime normal e regime promocional
(separadamente). Por fim, guardamos o número de linhas de venda do produto em
regime normal e regime promocional (separadamente).

## Faturação Base

```java
private double totalFaturado;
private int totalVendas;
private Map<String, FaturacaoProduto> produtos;
```

Esta estrutura associa a cada código de produto a sua estrutura da faturação
(`FaturacaoProduto`). Guarda ainda a informação do `totalFaturado` com vendas
desse produto e o número total de vendas (`totalVendas`).

## Faturação

```java
private List<List<FaturacaoBase>> registos;
```
Esta estrutura mantém organizada por filial e seguidamente por mês a faturação
de cada produto, ou seja, `List<List<FaturacaoBase>>` é uma lista com 3
elementos, `List<FaturacaoBase>` é um lista com 13 elementos, sendo que no
elemento de índice 0 está a faturação total, e nos restantes índices está a
faturação do mês correspondente.

![Diagrama explicativo da classe `Faturacao`](figures/faturacao.pdf)

## InfoProduto

```java
private IProduto produto;
private int quantidadeN;
private int quantidadeP;
private double faturacaoN;
private double faturacaoP;
```

Esta estrutura guarda a estrutura `IProduto` do produto. Guarda a quantidade de
produto transacionados em regime normal assim como a faturação que advém dos
mesmos e o mesmo para o regime promocional.

## Cliente Produtos

```java
private String codigoCliente;
private Map<String, InfoProduto> produtos;
```
Esta estrutura contém o código de cliente do cliente segundo o qual se guardam
informações. Para cada produto é guardada a informação sobre as suas compras
relativamente ao cliente.

## Filial

```java
private List<Map<String, ClienteProdutos>> registos;
```

Esta estrutura contém uma lista com 13 elementos. Em cada elemento da lista está
a associação de cada cliente à estrutura `ClienteProdutos`, ou seja, os produtos
comprados e as estruturas `InfoProduto` associadas a esses produtos. Na posição
de índice 0 da lista estão os resultados totais, nas restantes os resultados
mensais.

![Diagrama explicativo da classe `Filial`](figures/filial.pdf)

## GestVendasModel

```java
private ICatProdutos catProdutos;
private ICatClientes catClientes;
private IFaturacao facturacao;
private List<Filial> filiais;
private int nrVendasErradas;
private String ultimoFicheiroValido;
```

Esta é a classe que guarda a toda a informação da aplicação. Conta com um
catálogo de produtos, um catálogo de clientes, uma estrutura de faturação, uma
lista de filiais, um inteiro que contém o número de linhas de venda inválidas
que constam do ficheiro de vendas e contém a uma _string_ com o último ficheiro
válido.
