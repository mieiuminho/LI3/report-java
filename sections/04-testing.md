# Testes {#sec:testes}

## Leitura do Ficheiro de Vendas

Os resultados dos tempos de leitura sem _parsing_ dos ficheiros de vendas de 1
milhão, 3 milhões e 5 milhões são apresentados na figura e tabela seguinte.
Através de ambos, é possível comparar a performance entre usar a _class_
`BufferReader` e o método `readAllLines` presente na classe `Files`.

Fazendo a análise dos resultados, é possível concluir que existe um ganho usando
a `lerLinhasWithBuff` apenas no ficheiro de com 3 milhões de linhas de venda.

![Comparação entre `readLineWithBuff` e `readAllLines`](charts/reading.png)

| Ficheiro de Vendas (Leitura) | lerLinhasWithBuff | readAllLines |
| ---------------------------- | -----------------:| ------------:|
| 1 Milhão de Vendas           |             0.270 |        0.214 |
| 3 Milhões de Vendas          |             0.440 |        0.596 |
| 5 Milhões de Vendas          |             0.999 |        0.924 |

: Tempos (em segundos) de Leitura do ficheiro de vendas sem _parsing_

Nas medições incluindo o _parsing_ dos campos de uma venda e sua Construção, os
tempos entre ambos os métodos são equilibrados, sendo `readAllLines` sempre
ligeiramente melhor, e possivelmente justificados pelo percentagem menor que a
leitura tem nesta tarefa.

![Comparação entre `readLineWithBuff` e `readAllLines`](charts/parsing.png)

| Ficheiro de Vendas (Leitura + _Parsing_) | lerLinhasWithBuff | readAllLines |
| ---------------------------------------- | -----------------:| ------------:|
| 1 Milhão de Vendas                       |             1.093 |        0.637 |
| 3 Milhões de Vendas                      |             2.320 |        2.031 |
| 5 Milhões de Vendas                      |             3.663 |        3.321 |

: Tempos (em segundos) de Leitura do ficheiro de vendas com _parsing_ dos campos


Analisando os resultados da leitura, _parsing_ e validação de uma venda, as
diferenças entre ambos os métodos torna-se negligenciável uma vez que a sua
computação passa a ter menor peso na tarefa. Sendo os resultados pouco
conclusivos, o método `readAllLines` poderá ser mais vantajoso na maioria dos
casos.

![Comparação entre `readLineWithBuff` e `readAllLines`](charts/validating.png)

| Ficheiro de Vendas (Leitura + _Parsing_ + Validação) | lerLinhasWithBuff | readAllLines |
| ---------------------------------------------------- | -----------------:| ------------:|
| 1 Milhão de Vendas                                   |             1.012 |        0.929 |
| 3 Milhões de Vendas                                  |             3.256 |        3.541 |
| 5 Milhões de Vendas                                  |             4.936 |        5.503 |

: Tempos (em segundos) de Leitura do ficheiro de vendas com _parsing_ e
Validação  campos

\newpage

## Estruturas de Dados

| **Modelo**                      | **1M de Vendas**  | **3M de Vendas** | **5M de Vendas** |
| ------------------------------- | -----------------:| ----------------:| ----------------:|
| HashMap -- HashSet -- ArrayList |             3.631 |           12.999 |           47.438 |
| TreeMap -- HashSet -- Vector    |             8.897 |           35.987 |           73.803 |
| HashMap -- TreeSet -- Vector    |             4.814 |           17.247 |           35.492 |
| HashMap -- HashSet -- Vector    |             3.909 |           13.545 |           28.573 |
| TreeMap -- TreeSet -- ArrayList |            10.750 |           38.386 |           75.760 |
| TreeMap -- TreeSet -- Vector    |            11.785 |           40.665 |           75.533 |
| TreeMap -- HashSet -- ArrayList |             9.174 |           33.913 |           65.338 |
| HashMap -- TreeSet -- ArrayList |             4.901 |           17.246 |           60.687 |

: Tempos de Construção das Estruturas de Dados (Catálogo de Clientes e Produtos,
Faturação e Lista de Filiais)

![Tempos de reposta (em segundos) às Queries com 1 Milhão de Vendas](charts/queries_performance_1M.png)

![Tempos de reposta (em segundos) às Queries com 3 Milhões de Vendas](charts/queries_performance_3M.png)

![Tempos de reposta (em segundos) às Queries com 5 Milhões de Vendas](charts/queries_performance_5M.png)


