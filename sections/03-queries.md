# Método de resposta às _queries_ {#sec:queries}

## _Query_ 1

A _query_ 1 pretende determinar a lista de produtos que não são comprados. Para
isso, devolve uma lista de códigos de produto. Esta lista é obtida, percorrendo
a lista de produtos existentes no catálogo de produtos e verificando se existe
pelo menos uma compra do produto em pelo menos uma das filiais nos dados
associados na faturação. No fim, para obter o número de produtos não nunca
comprados, basta verificar o tamanho desta lista.

## _Query_ 2

De forma a responder a esta _query_, basta verificar quantas _keys_ (códigos de
cliente) existem em cada filial no mês indicado pelo utilizador. Para a segunda
parte, a resposta foi obtida indo à faturação verificar quantas vendas foram
realizadas nos mês indicado.

## _Query_ 3

A resposta a esta _query_, resulta do agrupamento numa estrutura auxiliar, por
mês, da quantidade de produtos comprados por um cliente, do seu total de
compras e o total gasto.

## _Query_ 4

Para responder a esta _query_ é seguido o método análogo à _query_ anterior, com
a única diferença de o agrupamento ser feito por código de produto e não de
cliente.

## _Query_ 5

Nesta _query_ é criado um conjunto de pares _Produto_ e _Quantidade_ que são
obtidos através de percorrer as vendas ocorridas por filial de um dado cliente.

## _Query_ 6

A _query_ 6 pretende dar os `X` produtos mais comprados em termos de quantidade
de produto. Para isso, são ordenados num conjunto de pares de _Produto_ e
_Quantidade_ ordenados pela quantidade de forma decrescente. Depois, são
devolvidos os `X` primeiros.

## _Query_ 7

Para esta _query_ é seguindo uma estratégia semelhante à _query_ anterior, mas é
criado uma lista para cada uma das filiais, em que cada uma contém os 3 maiores
compradores associados num par que associa um código de cliente à faturação
total gerada pelo mesmo.

## _Query_ 8

Nesta _query_ é listado os `X` clientes com uma variedade de produtos comprados
maior. Para tal, é criado um conjunto (desta forma evita-se repetidos) onde são
adicionados os produtos comprados por esse cliente em cada uma das filiais.

## _Query_ 9

O resultado desta _query_ é muito semelhante à anterior na forma de obtenção. No
entanto, uma vez que está restrito a um produto em especifico, a lista a
percorrer é menor.

## _Query_ 10

Por forma a responder à última _query_, para cada mês e cada filial, é criada
uma lista com um par de que associa um código de produto com a sua faturação.
Tal é obtido, indo ao mês correspondente da filial correspondente e somar e
obter a faturação corresponde a cada um dos produtos.

