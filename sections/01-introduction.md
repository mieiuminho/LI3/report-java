# Introdução {#sec:intro}

O desenvolvimento de *software* em larga escala introduz um conjunto de novos
problemas para o programador. Este tipo de programação requer, geralmente,
milhares de linhas de código para manipular estruturas de dados complexas que
vão manipular milhões de dados. Além disso, este trabalho deve ser feito em
equipa o que requer a capacidade de dividir o problema em partes concretas
complementares mas não inteiramente dependentes entre si.

Os princípios da Engenharia de *Software* acrescentam conceitos extremamente
relevantes para o desenvolvimento em larga escala. Em particular, o paradigma no
desenvolvimento torna-se orientado aos dados e foca-se na sua robustez e
segurança.

De forma a permitir um crescimento sustentável da complexidade do projeto foram
aplicados conceitos como a modularidade, reutilização, encapsulamento e
abstração de dados.

Este projeto foi desenvolvido na linguagem de programação orientada aos objetos
Java, seguindo os princípios anteriormente abordados, e consiste no
desenvolvimento de uma aplicação de terminal que permite a partir de ficheiros
de texto com informação relativa a clientes, produtos e vendas obter informações
relevantes na gestão de vandas num hipermercado com filiais.

O relatório está organizado do seguinte modo: na próxima secção serão descritas
as estruturas utilizadas. Na @sec:queries são explicados os métodos de obtenção
de resposta de cada *query*. A seguir são apresentados testes de performance.
Por fim, comentários conclusivos são apresentados na @sec:end.

