| Modelo                          | Query 5 | Query 6 | Query 7 | Query 8 | Query 9 |
| ------------------------------- | -------:| -------:| -------:| -------:| -------:|
| HashMap -- HashSet -- ArrayList | 0.00027 |   1.121 |   0.247 |   1.335 |   0.257 |
| TreeMap -- HashSet -- Vector    | 0.00053 |   1.164 |   0.195 |   1.396 |   0.266 |
| HashMap -- TreeSet -- Vector    | 0.00025 |   1.174 |   0.222 |   1.344 |   0.293 |
| HashMap -- HashSet -- Vector    | 0.00166 |   1.207 |   0.216 |   1.308 |   0.259 |
| TreeMap -- TreeSet -- ArrayList | 0.00029 |   1.200 |   0.216 |   1.407 |   0.281 |
| TreeMap -- TreeSet -- Vector    | 0.00293 |   1.334 |   0.257 |   1.600 |   0.312 |
| TreeMap -- HashSet -- ArrayList | 0.00037 |   1.172 |   0.210 |   1.518 |   0.291 |
| HashMap -- TreeSet -- ArrayList | 0.00048 |   1.169 |   0.223 |   1.403 |   0.285 |
