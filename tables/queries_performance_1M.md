| Modelo                          | Query 5 | Query 6 | Query 7 | Query 8 | Query 9 |
| ------------------------------- | -------:| -------:| -------:| -------:| -------:|
| HashMap -- HashSet -- ArrayList | 0.00013 |   1.144 |   0.103 |   0.535 |   0.068 |
| TreeMap -- HashSet -- Vector    | 0.00021 |   1.078 |   0.098 |   0.513 |   0.073 |
| HashMap -- TreeSet -- Vector    | 0.00022 |   1.032 |   0.097 |   0.501 |   0.074 |
| HashMap -- HashSet -- Vector    | 0.00018 |   0.905 |   0.405 |   0.390 |   0.070 |
| TreeMap -- TreeSet -- ArrayList | 0.00015 |   1.132 |   0.106 |   0.524 |   0.074 |
| TreeMap -- TreeSet -- Vector    | 0.00111 |   0.779 |   0.335 |   0.444 |   0.084 |
| TreeMap -- HashSet -- ArrayList | 0.00013 |   1.065 |   0.112 |   0.390 |   0.074 |
| HashMap -- TreeSet -- ArrayList | 0.00014 |   1.080 |   0.099 |   0.410 |   0.080 |
