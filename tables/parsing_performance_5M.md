| Modelo                          | Leitura |
| ------------------------------- | -------:|
| HashMap -- HashSet -- ArrayList |  47.438 |
| TreeMap -- HashSet -- Vector    |  73.803 |
| HashMap -- TreeSet -- Vector    |  35.492 |
| HashMap -- HashSet -- Vector    |  28.573 |
| TreeMap -- TreeSet -- ArrayList |  75.760 |
| TreeMap -- TreeSet -- Vector    |  75.533 |
| TreeMap -- HashSet -- ArrayList |  65.338 |
| HashMap -- TreeSet -- ArrayList |  60.687 |
