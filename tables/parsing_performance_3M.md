| Modelo                          | Leitura |
| ------------------------------- | -------:|
| HashMap -- HashSet -- ArrayList |  12.999 |
| TreeMap -- HashSet -- Vector    |  35.987 |
| HashMap -- TreeSet -- Vector    |  17.247 |
| HashMap -- HashSet -- Vector    |  13.545 |
| TreeMap -- TreeSet -- ArrayList |  38.386 |
| TreeMap -- TreeSet -- Vector    |  40.665 |
| TreeMap -- HashSet -- ArrayList |  33.913 |
| HashMap -- TreeSet -- ArrayList |  17.246 |
