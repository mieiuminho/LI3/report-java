| Modelo                          | Leitura |
| ------------------------------- | -------:|
| HashMap -- HashSet -- ArrayList |   3.631 |
| TreeMap -- HashSet -- Vector    |   8.897 |
| HashMap -- TreeSet -- Vector    |   4.814 |
| HashMap -- HashSet -- Vector    |   3.909 |
| TreeMap -- TreeSet -- ArrayList |  10.750 |
| TreeMap -- TreeSet -- Vector    |  11.785 |
| TreeMap -- HashSet -- ArrayList |   9.174 |
| HashMap -- TreeSet -- ArrayList |   4.901 |
