| Modelo                          | Query 5 | Query 6 | Query 7 | Query 8 | Query 9 |
| ------------------------------- | -------:| -------:| -------:| -------:| -------:|
| HashMap -- HashSet -- ArrayList | 0.00060 |   1.369 |   0.971 |   2.420 |   0.513 |
| TreeMap -- HashSet -- Vector    | 0.00510 |   1.025 |   1.265 |   2.285 |   0.510 |
| HashMap -- TreeSet -- Vector    | 0.00055 |   0.974 |   0.839 |   2.369 |   0.511 |
| HashMap -- HashSet -- Vector    | 0.00053 |   0.960 |   0.852 |   2.364 |   0.534 |
| TreeMap -- TreeSet -- ArrayList | 0.00038 |   0.952 |   1.072 |   2.453 |   0.547 |
| TreeMap -- TreeSet -- Vector    | 0.00479 |   1.168 |   1.045 |   2.370 |   0.509 |
| TreeMap -- HashSet -- ArrayList | 0.00041 |   1.819 |   0.370 |   3.097 |   0.540 |
| HashMap -- TreeSet -- ArrayList | 0.00161 |   1.189 |   0.812 |   2.329 |   0.511 |
